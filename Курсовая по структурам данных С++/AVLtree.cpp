#include "AVLtree.h"
#include "funcs.h"
#include <iostream>
#include <iomanip>


using namespace std;



Doctor::Doctor()
{
	fill(doctorName, doctorName + docNameLen, 0);
	fill(doctorPosition, doctorPosition + charLen, 0);
	fill(workingHours, workingHours + charLen, 0);
	officeNo = 0;
}

AVLtree::AVLtree(Doctor doctorData) 
{ 
	element = doctorData; 
	left = right = 0; 
	height = 1;
}

unsigned char height(AVLtree* p)
{
	return p ? p->height : 0; //(�������) ? true : false
}

int balFact(AVLtree* p)
{
	return height(p->right) - height(p->left);
}

void fixHeight(AVLtree* p)
{
	unsigned char hl = height(p->left);
	unsigned char hr = height(p->right);
	p->height = (hl > hr ? hl : hr) + 1;
}

AVLtree* rightRot(AVLtree* p)
{
	AVLtree*q = p->left;
	p->left = q->right;
	q->right = p;
	fixHeight(p);
	fixHeight(q);
	return q;
}

AVLtree* leftRot(AVLtree *q)
{
	AVLtree*p = q->right;
	q->right = p->left;
	p->left = q;
	fixHeight(q);
	fixHeight(p);
	return p;
}

AVLtree* balance(AVLtree* p)
{
	fixHeight(p);
	if (balFact(p) == 2)
	{
		if (balFact(p->right) < 0)
			p->right = rightRot(p->right);
		return leftRot(p);
	}
	if (balFact(p) == -2)
	{
		if (balFact(p->left) > 0)
			p->left = leftRot(p->left);
		return rightRot(p);
	}
	return p;
}

AVLtree* insert(AVLtree* p, Doctor doctorData)
{
	if (!p)
	{
		return new AVLtree(doctorData);
	}
	if (strcmp(doctorData.doctorName, p->element.doctorName) < 0)
		p->left = insert(p->left, doctorData);
	else
		p->right = insert(p->right, doctorData);
	return balance(p);
}

AVLtree* findMin(AVLtree* p)
{
	return p->left ? findMin(p->left) : p;
}

AVLtree* delMin(AVLtree* p)
{
	if (p->left == 0)
		return p->right;
	p->left = delMin(p->left);
	return balance(p);
}

AVLtree* del(AVLtree* p, char* doctorName, bool* isDone)
{
	if (!p) return 0;
	if (strcmp(doctorName, p->element.doctorName) < 0)
		p->left = del(p->left, doctorName, isDone);
	else if (strcmp(doctorName, p->element.doctorName) > 0)
		p->right = del(p->right, doctorName, isDone);
	else
	{
		AVLtree* q = p->left;
		AVLtree* r = p->right;
		delete p;
		if (!r) return q;
		AVLtree* min = findMin(r);
		min->right = delMin(r);
		min->left = q;
		*isDone = true;
		return balance(min);
	}
	*isDone = false;
	return balance(p);
}

bool out(AVLtree* p, bool& temp)
{
	if (p) {
		out(p->left, temp);
		p->element.print();
		out(p->right, temp);
		temp = true;
	}

	return temp;
}

void outTest(AVLtree* p, int i)
{
	if (p)
	{
		outTest(p->left, i + 1);
		cout << i;
		p->element.print();
		cout << endl;
		outTest(p->right, i + 1);
	}
}

void delAll(AVLtree** p)
{
	if (*p) {
		delAll(&((*p)->left));
		delAll(&((*p)->right));
		delete *p;
	}
	
}

void searchName(AVLtree* p, char* doctorName, bool& isTrue)
{

	if (p)
	{
		if (strcmp(doctorName, p->element.doctorName) < 0)
		{
			searchName(p->left, doctorName, isTrue);
		}
		else if (strcmp(doctorName, p->element.doctorName) > 0)
		{
			searchName(p->right, doctorName, isTrue);
		}
		else
		{
			p->element.print();
			isTrue = true;
		}
	}
}

char* searchWorkingHours(AVLtree* p, char* doctorName)
{
	if (p)
	{
		if (strcmp(doctorName, p->element.doctorName) < 0)
		{
			searchWorkingHours(p->left, doctorName);
		}
		else if (strcmp(doctorName, p->element.doctorName) > 0)
		{
			searchWorkingHours(p->right, doctorName);
		}
		else
		{
			return p->element.workingHours;
		}
	}
}

bool isExistSearch(AVLtree* p, char*doctorName, bool& temp)
{
	if (p)
	{
		if (strcmp(doctorName, p->element.doctorName) < 0)
			isExistSearch(p->left, doctorName, temp);
		else if (strcmp(doctorName, p->element.doctorName) > 0)
			isExistSearch(p->right, doctorName, temp);
		else
		{
			temp = true;
		}
	}
	return temp;
}

bool searchPos(AVLtree* p, char* doctorPos, bool& temp)
{
	if (p)
	{
		searchPos(p->left, doctorPos, temp);
		searchPos(p->right, doctorPos, temp);
		if (strDirFind(p->element.doctorPosition, doctorPos) != 0)
		{
			p->element.print();
			temp = true;
		}
	}
	return temp;
}

void Doctor::print()
{
	cout << endl;
	cout << "���:                 " << doctorName << endl;
	cout << "���������:           " << doctorPosition << endl;
	cout << "���� ������:         " << workingHours << endl;
	cout << "����� ��������:      " << officeNo << endl << endl;
}

void inputDoctor(AVLtree* tempNode, Doctor* doctorData)
{

	bool T = 0;
	int time1 = 0, time2 = 0;
	char temp[charLen]{ "0" };

	cout << "\n������� ������� � �������� �����: " << endl << endl;
	while (!T)
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin.getline(doctorData->doctorName, '/0');
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� �������� a=" << endl;
			cin.getline(doctorData->doctorName, '/0');
		}

		for (size_t i = 0; i < docNameLen; i++)
		{

			if (isdigit(static_cast<unsigned char>(doctorData->doctorName[i])) == 0)
				T = 1;
			else
			{
				cout << endl << "������! ��������� ����!" << endl;
				T = 0;
				break;
			}
		}
		bool temp = false;
		if (isExistSearch(tempNode, doctorData->doctorName, temp))
		{
			cout << endl << "������ � ����� ������ ��� ���������������." << endl;
			cout << "��������� ��������, ����� ��� ���� ����������.    " << endl;
			T = 0;
		}
	}

	T = 0;

	cout << "\n������� ��������� �����: " << endl << endl;
	while (!T)
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin.getline(doctorData->doctorPosition, '/0');
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� �������� a=" << endl;
			cin.getline(doctorData->doctorPosition, '/0');
		}

		for (int i = 0; i < charLen - 1; i++)
		{

			if (isdigit(static_cast<unsigned char>(doctorData->doctorPosition[i])) == 0)
				T = 1;
			else
			{
				T = 0;
				break;
			}
		}

		if (!T)
			cout << "\n������! ��������� ����!" << endl;
	}

	T = 0;

	cout << "\n������� ���� ������: " << endl;
	cout << "������ �����: ���� �� 0 �� 23" << endl;

	while (!T)
	{
		cout << "\n������� ����� ������ ������: " << endl;

		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin >> time1;

		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: " << endl;
			cin >> time1;
		}

		cout << "\n������� ����� ����� ������: " << endl;

		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin >> time2;

		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: " << endl;
			cin >> time1;
		}

		if (time1 > 23 || time1 < 0 || time2>23 || time2 < 0)
		{
			T = 0;
			cout << "������! ������� ������ ��������! " << endl << endl;
		}
		else T = 1;
	}

	if (time1 < 10)
	{
		temp[0] = '0';
		temp[1] = '0' + time1;
	}
	else
		for (int i = 1; i >= 0; i--)
		{
			temp[i] = '0' + time1 % 10;
			time1 /= 10;
		}

	strcat_s(temp, "-");

	if (time2 < 10)
	{
		temp[3] = '0';
		temp[4] = '0' + time2;
	}
	else
		for (int i = 4; i > 2; i--)
		{
			temp[i] = '0' + time2 % 10;
			time2 /= 10;
		}

	temp[5] = '\0';
	strcpy_s(doctorData->workingHours, temp);

	T = 0;

	while (!T)
	{
		cout << "������� ����� �������� ��������: " << endl;
		cout << "� ����������� ���� 50 ���������." << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin >> doctorData->officeNo;

		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: " << endl;
			cin >> doctorData->officeNo;
		}

		if (doctorData->officeNo <= 0 || doctorData->officeNo > 50)
		{
			T = 0;
			cout << "������! ������� ������ ��������! " << endl;
		}
		else T = 1;
	}
}
