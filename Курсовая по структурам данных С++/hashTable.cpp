#include <iostream>
#include <windows.h>

#include "funcs.h"
#include "hashTableClass.h"
#include "globals.h"

using namespace std;

Patient::Patient()
{
	isEmpty = true;
	birthDate = 0;
	fill(regNumber, regNumber + C, 0);
	fill(patientName, patientName + charLen, 0);
	fill(adress, adress + charLen, 0);
	fill(job, job + charLen, 0);
}

void Patient::print(int i)
{
	cout << endl;
	cout << "\n��������������� �����: " << regNumber << endl;
	cout << "��� ��������:          " << patientName << endl;
	if (i == 1)
	{
		cout << "���� ��������:         " << birthDate << endl;
		cout << "����� ����������:      " << adress << endl;
		cout << "����� ������:          " << job << endl << endl;
	}
}

void Patient::inputPatient()
{
	/*if (regNo >= 999999) regNo = 1;
	else regNo += 1;

	char* data = new char[10]{ "" };
	bool T = 0;

	while (!T)
	{
		cout << "\n������� ����� ������� (����� ������ �������� �� 2-� ����): " << endl;

		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin >> data;
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: ";
			cin >> data;
		}

		if (strlen(data) != 2)
		{
			T = 0;
			cout << "������! �������� ����� ������! " << endl;

		}

		for (int i = 0; i < 2; i++)
		{
			if (isdigit(static_cast<unsigned char>(data[i])) != 0)
			{
				T = 1;
			}
			else
			{
				T = 0;
				break;
			}
		}

	}

	char buffer[buffer_size] = { '0','0','0','0','0','0' };
	int temp = regNo;
	int num_order = Order(regNo);

	for (int i = buffer_size - 2; i >= 0; i--)
	{
		buffer[i] = '0' + temp % 10;

		temp /= 10;
	}

	strcat_s(data, 4, "-");
	strcat_s(data, 10, buffer);

	for (int i = 0; i < C; i++)
	{

		regNumber[i] = data[i];
	}
	delete[] data;

	T = 0;
	*/
	bool T = 0;

	cout << "\n������� ��������������� ����� ��������: " << endl;
	cout << "������ ���������������� ������: ��-������" << endl;
	cout << "��� �� - ����� �������, ������ - ���������� �����." << endl;
	while (!T)
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin.getline(regNumber, '/0');
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������� ��������� �������� a=" << endl;
			cin.getline(regNumber, '/0');
		}
		if (strlen(regNumber) != C - 1)
			T = 0;
		else
		{
			for (int i = 0; i < C - 1; i++)
			{
				if (i == 2)
					if (regNumber[i] == '-')
						T = 1;
					else
					{
						T = 0;
						break;
					}
				else if (isdigit(static_cast<unsigned char>(regNumber[i])) != false)
					T = 1;
				else
				{
					T = 0;
					break;
				}
			}
		}

		if (!T)
			cout << "\n�������� ������ �����, ��������� ����!" << endl;
	}
	T = 0;
	while (!T)
	{
		cout << "������� ��� ��������:  " << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin.getline(patientName, '/0');
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: " << endl;
			cin.getline(patientName, '/0');
		}

		for (size_t i = 0; i < strlen(patientName); i++)
		{
			if (isdigit(static_cast<unsigned char>(patientName[i])) == 0)
			{
				T = 1;
			}
			else
			{
				T = 0;
			}
		}
	}

	T = 0;

	while (!T)
	{
		cout << "������� ��� �������� ��������: " << endl;
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin >> birthDate;

		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: " << endl;
			cin >> birthDate;
		}

		if (birthDate > 2019 || birthDate < 1900)
		{
			T = 0;
			cout << "������! ������� ������ ��������! " << endl;
		}
		else T = 1;
	}

	cout << "������� ����� ��������:  " << endl;
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	cin.getline(adress, '/0');
	while (cin.fail())
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cout << "������! ������� ��������� ��������: " << endl;
		cin.getline(patientName, '/0');
	}

	cout << "������� ����� ������ ��������:  " << endl;
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	cin.getline(job, '/0');
	while (cin.fail())
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cout << "������! ������� ��������� ��������: " << endl;
		cin.getline(patientName, '/0');
	}
}

HashTable::HashTable(Patient patient)
{
	Patient patientTable [N];
	for (int i = 0; i < N; i++)
	{
		
		patientTable[i] = patient;
		deleted[i] = false;
		numOfCollisions[i] = 0;
		numOfBad[i] = 0;
	}
}

bool HashTable::isThereAnybody()
{
	for (u_int i = 0; i < N; i++)
	{
		if (patientTable[i].isEmpty != true)
			return true;
	}
	return false;
}

bool HashTable::allpatientDataOut()
{
	for (u_int i = 0; i < N; i++)
	{
		if (patientTable[i].isEmpty != true)
		{
			if (deleted[i] == 1)
				cout << endl << i << ". �������.";
			else
			{
				cout << endl << i << ". " << endl;
				patientTable[i].print(1);
			}
		}
	}
	return isThereAnybody();
}

void HashTable::add(Patient patient)
{
	u_int x = hashFunc(patient.regNumber);
	int count = 0;

	for (int i = 0; i < N; i++)
	{
		if (patientTable[x].isEmpty == true)
		{
			patientTable[x] = patient;
			deleted[x] = false;
			patientTable[x].isEmpty = false;
			break;
		}

		if (patientTable[x].isEmpty == false && deleted[x] == 1)
		{
			patientTable[x] = patient;
			deleted[x] = false;
			patientTable[x].isEmpty = false;
			break;
		}

		if (patientTable[x].isEmpty == false && strcmp(patientTable[x].regNumber, patient.regNumber) == 0 && deleted[x] != 1)
			if (strcmp(patientTable[x].regNumber, patient.regNumber) == 0)
			{
				cout << "����� ���� ��� ���� � �������!";
				break;
			}
		if ((patientTable[x].isEmpty == false) && (strcmp(patientTable[x].regNumber, patient.regNumber) != 0))
		{
			x = (x + 3 * i) % N;
			numOfCollisions[x] += 1;
			if (numOfCollisions[x] > 30 || x > N)
			{
				numOfBad[x] += 1;
				cout << "\n�� ������� �������� �������!";
				break;
			}
		}
	}
}

Patient HashTable::search(char* regNumb, bool* isDone)
{
	u_int x = hashFunc(regNumb);
	int count = 0;

	for (int i = 0; i < N; i++)
	{
		if (patientTable[x].isEmpty == false)
		{
			if (strcmp(patientTable[x].regNumber, regNumb) == 0 && deleted[x] != true)
			{
				*isDone = true;
				return patientTable[x];
			}
			else
			{
				x = (x + 3 * i)%N;
				
				count++;
				if (count > 30)
				{
					*isDone = false;
					return patientTable[x];
				}
			}
		}
		else
		{
			*isDone = false;
			return patientTable[x];
		}
	}
}

bool HashTable::search(char* regNumb)
{
	u_int x = hashFunc(regNumb);
	int count = 0;

	for (int i = 0; i < N; i++)
	{
		if (patientTable[x].isEmpty == false)
		{
			if (strcmp(patientTable[x].regNumber, regNumb) == 0 && deleted[x] != true)
			{
				return true;
			}
			else
			{
				x = (x + 3 * i)%N;
				count++;
				if (count > 30)
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
	}
}

bool HashTable::search(char* name, Patient* temp)
{
	bool isDone = false;
	for (int i = 0; i < N; i++)
	{
		if (patientTable[i].isEmpty == false)
		{

			if (strDirFind(patientTable[i].patientName, name) == 1 && deleted[i] != true)
			{
				cout << i << ". " << endl;
				patientTable[i].print(0);
				cout << endl<<endl;
				isDone = true;
			}
		}
	}
	return isDone;
}

bool HashTable::remove(char* key)
{
	bool done = false;
	u_int x = hashFunc(key);
	int count = 0;

	for (int i = 0; i < N; i++)
	{
		if (patientTable[x].isEmpty == false && deleted[x] != true)
			if (strcmp(patientTable[x].regNumber, key) == 0)
			{
				deleted[x] = true;
				done = true;
				return done;
				break;
			}
			else
				x = (x + 3 * i)%N;
		else
		{
			x = (x + 3 * i) % N;
			count++;
			if (count > 30)
			{
				return done;
				break;
			}
		}
	}
}

void HashTable::allpatientDataDel()
{
	for (int i = 0; i < N; i++)
	{
		patientTable[i].isEmpty = true;
		deleted[i] = false;
		numOfCollisions[i] = 0;
		numOfBad[i] = 0;
	}
	regNo = 0;
}

