#include <windows.h>
#include <iostream>
#include "globals.h"

using namespace std;


u_int hashFunc(char* regNumber)
{
	u_int x = 0;
	for (int i = 0; i < C; i++)
		x = x + static_cast<u_int>(regNumber[i]);
	x = 7*(x - 429)%N;
	return x;
}

int order(int num)
{
	int order = 0;

	while (num) {
		num /= 10;

		++order;
	}
	return order;
}

void inputRegNumber(char* temp)
{
	bool T = 0;

	cout << "\n������� ��������������� ����� ��������: " << endl;
	cout << "������ ���������������� ������: ��-������" << endl;
	cout << "��� �� - ����� �������, ������ - ���������� �����." << endl;
	while (!T)
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin.getline(temp, '/0');
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������� ��������� �������� a=" << endl;
			cin.getline(temp, '/0');
		}
		if (strlen(temp) != C - 1)
			T = 0;
		else
		{
			for (int i = 0; i < C - 1; i++)
			{
				if (i == 2)
					if (temp[i] == '-')
						T = 1;
					else
					{
						T = 0;
						break;
					}
				else if (static_cast<unsigned char>(isdigit(temp[i])) != 0)
					T = 1;
				else
				{
					T = 0;
					break;
				}
			}
		}

		if (!T)
			cout << "\n�������� ������ �����, ��������� ����!" << endl;
	}
}

void inputChar(char* temp)
{
	bool T = 0;
	while (!T)
	{
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		cin.getline(temp, '/0');
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(cin.rdbuf()->in_avail());
			cout << "������! ������� ��������� ��������: " << endl;
			cin.getline(temp, '/0');
		}

		for (size_t i = 0; i < strlen(temp); i++)
		{
			if (isdigit(static_cast<unsigned char>(temp[i])) != true)
			{
				T = 1;
			}
			else
			{
				T = 0;
				break;
			}
		}
	}
}

int menu()
{
	int choice;

	cout << endl <<
    	"\n\t  ____________________________________" << endl <<
		"\t |1 - ������ � ��������               |" << endl <<
		"\t |2 - ������ � �������                |" << endl <<
		"\t |3 - ����������� ������ �����������  |" << endl <<
		"\t |4 - ����������� �������� �����������|" << endl <<
		"\t |5 - ����� �����������               |" << endl <<
		"\t |6  - �����                          |" << endl <<
		"\t |____________________________________|\n\n" << endl << endl;

	cout << "\t\t�������� �����\n" << endl;

	choice = _getwch();

	return choice;
}

int subMenupatientData()
{
	int choice;

	cout << endl <<
		"\n\t  _______________________________" << endl <<
		"\t |1 - ����������� ������ ��������|" << endl <<
		"\t |2 - �������� ������ � �������  |" << endl <<
		"\t |3 - �������� ���� �������      |" << endl <<
		"\t |4 - ������� ������ � �������   |" << endl <<
		"\t |5 - ����� �������� �� ���.�    |" << endl <<
		"\t |6 - ����� �������� �� ���      |" << endl <<
		"\t |7 - �����                      |" << endl <<
		"\t |_______________________________|\n\n" << endl << endl;

	cout << "\t\t�������� �����\n" << endl;

	choice = _getwch();

	return choice;
}

int subMenuDoctors()
{
	int choice;

	cout << endl <<
		"\n\t  _______________________________" << endl <<
		"\t |1 - ���������� ������ �����    |" << endl <<
		"\t |2 - �������� ������ � �����    |" << endl <<
		"\t |3 - �������� ���� ������       |" << endl <<
		"\t |4 - ������� ������ � ������    |" << endl <<
		"\t |5 - ����� ����� �� ���         |" << endl <<
		"\t |6 - ����� ����� �� ���������   |" << endl <<
		"\t |7 - �����                      |" << endl <<
		"\t |_______________________________|\n\n" << endl << endl;

	cout << "\t\t�������� �����\n" << endl;

	choice = _getwch();

	return choice;
}

int strDirFind(char* str1, char* str2)
{
	int i = -1, j;
	int stringLen = static_cast<int>(strlen(str1));
	int keyLen = static_cast<int>(strlen(str2));
	
	do
	{
		j = 0;
		i++;
		while ((j < keyLen) && (str1[i + j] == str2[j])) j++;
	} while (j < keyLen && i < stringLen - keyLen);

	if (j == keyLen)
		return 1;
	else return 0;
}
