#include "hashTableClass.h"
#include "AVLtree.h"
#include "globals.h"

#ifndef __SKIP_LIST_H__
#define __SKIP_LIST_H__

using namespace std;



class SkipList
{

public:
	struct Refferal
	{
		char regNumber[C];
		char doctorName[docNameLen];
		char date[charLen];
		char time[charLen];

		void print();
		void inputRefferal(HashTable*, AVLtree*, SkipList*, int);
		Refferal();

	};

private:
	struct SkipListNode
	{
		Refferal item;
		int level;
		SkipListNode** next;
		SkipListNode(Refferal x, int k)
		{
			item = x; level = k; next = new SkipListNode*[k];
			for (int i = 0; i <= k; i++) next[i] = 0;
		}
	};



private:
	bool searchPout(SkipListNode* link, char* key, HashTable* table, bool& isDone);
	bool searchPDT(SkipListNode* link, char* key, char* date, char* time, bool& isDone);
	int random();
	void insertP(SkipListNode* link, SkipListNode* item, int currentLevel);
	bool removeP(SkipList::Refferal& temp,  SkipListNode* link, int currLevel, bool& isDone);
	void searchP(SkipListNode* link, char* key, int i, int currLevel);

public:
	SkipListNode* head;
	Refferal nullItem;
	int maxLevel;
	int currentLevel;

	

	bool searchOut(char* key, HashTable* table);
	bool searchN(char* key);
	bool searchDT(char* key, char* date, char* time);
	void search(char* key);
	void insert(Refferal item);
	bool remove(SkipList::Refferal& temp);
	bool print();
	void removeAll();

	SkipList();
	~SkipList();

};

#endif //__SKIP_LIST_H__
