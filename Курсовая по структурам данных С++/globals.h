#include <windows.h>

#ifndef __GLOBALS_H__
#define __GLOBALS_H__

int const buffer_size = 7;
u_int const N = 512;
int const charLen = 50;
int const docNameLen = 26;
int const C = 10;
extern int regNo;

#endif // __GLOBALS_H__
