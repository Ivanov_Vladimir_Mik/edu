#include "globals.h"

#ifndef __HASH_TABLE_H__
#define __HASH_TABLE_H__

struct Patient
{

	char regNumber[C];
	char patientName[charLen];
	int birthDate;
	char adress[charLen];
	char job[charLen];
	bool isEmpty;

	void print(int i);
	void inputPatient();
	Patient();
};

class HashTable
{
public:

	Patient patientTable[N];
	bool deleted[N];
	int numOfCollisions[N];
	int numOfBad[N];

	HashTable(Patient patient);

	void add(Patient);
	bool remove(char*);
	Patient search(char*, bool*);
	bool search(char*, Patient*);
	bool search(char* regNumb);
	bool allpatientDataOut();
	void allpatientDataDel();
	bool isThereAnybody();
};

#endif //__HASH_TABLE_H__