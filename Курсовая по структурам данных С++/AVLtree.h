#include "globals.h"


#ifndef __AVL_TREE_H__
#define __AVL_TREE_H__

struct Doctor
{
	char doctorName[docNameLen];
	char doctorPosition[charLen];
	char workingHours[charLen];
	int officeNo;

	void print();
	
	Doctor();
};

struct AVLtree
{
	Doctor element;
	unsigned char height;
	AVLtree* left;
	AVLtree* right;

	AVLtree(Doctor);
};

unsigned char height(AVLtree*);

void inputDoctor(AVLtree*, Doctor*);
int balFact(AVLtree*);
void fixHeight(AVLtree*);
AVLtree* rightRot(AVLtree*);
AVLtree* leftRot(AVLtree*);
AVLtree* balance(AVLtree*);
AVLtree* insert(AVLtree*, Doctor);
AVLtree* findMin(AVLtree*);
AVLtree* delMin(AVLtree*);
AVLtree* del(AVLtree*, char*, bool*);
void outTest(AVLtree*, int i);
bool out(AVLtree*, bool&);
void delAll(AVLtree**);
void searchName(AVLtree*, char*, bool&);
bool searchPos(AVLtree*, char*, bool&);
char* searchWorkingHours(AVLtree*, char*);
bool isExistSearch(AVLtree*, char*, bool&);


#endif // __AVL_TREE_H__
