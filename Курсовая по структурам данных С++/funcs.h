#include <windows.h>

#ifndef __FUNCS_H__
#define __FUNCS_H__

int order(int);
u_int hashFunc(char*);
void inputRegNumber(char*);
void inputChar(char*);
int menu();
int subMenupatientData();
int subMenuDoctors();
int strDirFind(char*, char*);

#endif // __FUNCS_H__
