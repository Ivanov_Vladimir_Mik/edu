﻿#include <stdlib.h> 
#include <crtdbg.h> 
#include <vector>

#include <iostream>
#include <windows.h>
#include "globals.h"
#include "funcs.h"
#include "globals.h"
#include "hashTableClass.h"
#include "AVLtree.h"
#include "skipList.h"

using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	setlocale(LC_ALL, "Russian");

	Patient patientData;
	Doctor doctorData;
	HashTable patientTable(patientData);
	AVLtree* doctorTree = 0;
	SkipList::Refferal refferalData;
	SkipList refferalList;

	int userMenu;
	int choice = 0;
	char key[charLen];
	bool isDone = false;

	do {
		userMenu = menu();

		switch (userMenu)
		{
		case 0x31:
			do {
				userMenu = subMenupatientData();
				switch (userMenu)
				{
				case 0x31:
					patientData.inputPatient();
					patientTable.add(patientData);
					break;
				case 0x32:
					inputRegNumber(key);
					if (patientTable.remove(key) == true)
						cout << "Запись удалена! " << endl;
					else
						cout << "Такой записи не найдено! " << endl;
					break;
				case 0x33:
					if (!patientTable.allpatientDataOut())
						cout << "Не найдено ни одной записи! " << endl;
					break;
				case 0x34:
					patientTable.allpatientDataDel();
					refferalList.removeAll();
					cout << "Данные о пациентах удалены! " << endl;
					break;
				case 0x35:
					cout << endl;
					inputRegNumber(key);
					patientData = patientTable.search(key, &isDone);
					if (isDone == false)
						cout << "Такой записи не существует!" << endl;
					else
					{
						patientData.print(1);

						cout << "Выданные направления: " << endl << endl;
						if (!refferalList.searchN(key))
						cout << "Направлений не зарегистрировано. " << endl << endl;
					}
					break;
				case 0x36:
					cout << "Введите ФИО пациента: " << endl << endl;

					inputChar(key);
					isDone = patientTable.search(key, &patientData);
					if (isDone == false)
						cout << "Записей не найдено!" << endl;
					break;
				case 0x37:
					break;
				default:
					break;
				} 
			} while (userMenu != 0x37);

			break;
		case 0x32:
			do {
				userMenu = subMenuDoctors();
				switch (userMenu)
				{
				case 0x31:
					inputDoctor(doctorTree, &doctorData);
					doctorTree = insert(doctorTree, doctorData);
					break;
				case 0x32:
					cout << "Введите ФИО врача: " << endl;
					inputChar(key);
					doctorTree = del(doctorTree, key, &isDone);
					if (!isDone) cout << "Запись не найдена! " << endl;
					else cout << "Запись удалена! " << endl;
					break;
				case 0x33:
					isDone = false;
					//outTest(doctorTree, 0);
					if (!out(doctorTree, isDone))
						cout << "Записей не найдено! " << endl;
					break;
				case 0x34:
					delAll(&doctorTree);
					doctorTree = nullptr;
					refferalList.removeAll();
					cout << "Все данные удалены! " << endl;
					break;
				case 0x35:
					isDone = false;
					cout << "Введите ФИО врача: " << endl;
					inputChar(key);
					
					searchName(doctorTree, key, isDone);
					if (!isDone)
						cout << endl << "Такой врач не зарегистрирован." << endl;
					else
					{
						cout << endl << "Пациенты, имеющие направление к этому врачу: " << endl;

						if (!refferalList.searchOut(key, &patientTable))
							cout << endl << "Ни одного направления к этому врачу не зарегистрировано! " << endl;
					}
					
					break;
				case 0x36:
					isDone = false;
					cout << "Введите должность врача, или ее часть: " << endl;
					inputChar(key);
					if (!searchPos(doctorTree, key, isDone))
						cout << endl << "Врачей с такой должностью не зарегистрировано." << endl;
					break;
				case 0x37:
					break;
				default:
					break;
				}
			} while (userMenu != 0x37);
			
			break;
		case 0x33:
			if (refferalList.head == nullptr)
			refferalList.SkipList::SkipList();

			if ((patientTable.isThereAnybody()) && doctorTree) {
				refferalData.inputRefferal(&patientTable, doctorTree, &refferalList, 0);
				refferalList.insert(refferalData);
				cout << "Направление добавлено. " << endl;
			}
			else cout << "Нет зарегистрированных пациентов или врачей." << endl;
			break;
		case 0x34:
			
			cout << "Для закрытия направления введите следующие данные: " << endl << endl;
			
			refferalData.inputRefferal(&patientTable, doctorTree, &refferalList, 1);
			
			if (refferalList.remove(refferalData))
			{
				cout << "Направление возвращено!" << endl;
			}
			
			else	cout << "Такого направления не зарегистрировано! " << endl;
			
			
			//cout << "Введите имя врача: " << endl;

			//cin >> key;
			//refferalList.search(key);
			break;

		case 0x35:
			
			if (!refferalList.print())
				cout << "В данный момент не выдано ни одного направления." << endl;
			break;
		case 0x36:
			break;
		default:
			break;
		}
	} while (userMenu != 0x36);

	if (doctorTree)
		delAll(&doctorTree);

		refferalList.removeAll();

		patientTable.allpatientDataDel();

	system("pause");

	
	return 0;
}

