//: com/tsystems/javaschool/tasks/calculator/Calculator.java

package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

/**
 * @author Vladimir Ivanov
 * @version 1.0
 */
public class Calculator {

    private static String supportedOperators = "-+*/()";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {



        //null and empty input checks
        if (statement == null || statement.isEmpty()) return null;

        //removes all space characters from input string
        statement = statement.replaceAll("\\s","");

        //correct input check
        if (!InputProcessor.isCorrectExpr(statement)) return null;

        //split String statement to queue of operands and operators
        ArrayList<String> tokens = InputProcessor.splitter(statement, supportedOperators);

        /*convert from infix to postfix or return NULL if fails*/
        Deque<String> postfixExprStack = new ArrayDeque<String>();

        try {
            postfixExprStack = DijkstraAlgorithm.infixToPostfixTransform(tokens);
        } catch (CannotConvertIntoPostfixException e) {
            return null;
        }

        //calculate expression
        float rightOperand, leftOperand;
        Deque<Float> calcQue = new ArrayDeque<Float>();

        while (!postfixExprStack.isEmpty()) {

            if (isOperator(postfixExprStack.peek())) { //if the next token is an operator - take two last tokens from calculation que and calculate an expression using this operator

                try {
                    rightOperand = calcQue.pollLast();
                    leftOperand = calcQue.pollLast();
                }
                catch (NullPointerException e) {
                    return null;
                }

                switch (postfixExprStack.pop()) {
                    case "-": {
                        calcQue.addLast(leftOperand - rightOperand);
                        break;
                    }
                    case "+": {
                        calcQue.addLast(leftOperand + rightOperand);
                        break;
                    }
                    case "/": {
                        if (rightOperand == 0.)
                            return null;
                        calcQue.addLast(leftOperand / rightOperand);
                        break;
                    }
                    case "*": {
                        calcQue.addLast(leftOperand * rightOperand);
                        break;
                    }
                    default:
                        return null;
                }
            } else calcQue.addLast(Float.parseFloat(postfixExprStack.pop())); // if the next token is an operand - add it to the calculation que
        }

        if (calcQue.size() != 1) return null; //if there was a mistake in postfix notation the result size of a que may be incorrect
        else {
            float result = Math.round(calcQue.pollLast() * 10000f) / 10000f; //rounds result to 4 decimal places
            if (result % 1 == 0)

                //returns result as int if result hasn't decimal part or as float otherwise
                return (int) result + "";
            return result + "";
        }
    }

    //returns true if token is operator or false in other cases
    private static boolean isOperator(String token) {
        return (supportedOperators.contains(token));
    }

    private static class InputProcessor {
        //input string pattern matching check
        private static boolean isCorrectExpr(String statement) {
            if (!statement.matches("^(([(]?)*\\d+([.]\\d*)?)([-+/*]([(]?)*\\d+([.]\\d*)?([)]?)*)*$"))
                return false;

            int scopesCheck = 0;

            for (char ch : statement.toCharArray()) {
                if (ch == '(') scopesCheck++;
                else if (ch == ')') scopesCheck--;
            }

            return (scopesCheck == 0); //checks if scopes are always closed
        }

        //splits input string by specified delimiters
        private static ArrayList<String> splitter(String statement, String delimiters) {
            StringTokenizer tokenizer = new StringTokenizer(statement, delimiters, true);
            ArrayList<String> tokens = new ArrayList<String>(statement.length());

            while (tokenizer.hasMoreTokens())
                tokens.add(tokenizer.nextToken().trim());

            return tokens;
        }
    }

    private static class DijkstraAlgorithm {

        //converts input expression into postfix form
        private static Deque<String> infixToPostfixTransform(ArrayList<String> tokens) throws CannotConvertIntoPostfixException {
            Deque<String> postfixExprStack = new ArrayDeque<String>();
            Deque<String> tempOperatorsStack = new ArrayDeque<String>();

            for (String token : tokens) {
                if (!isOperator(token)) postfixExprStack.addLast(token);
                else if (token.equals("(")) {
                    tempOperatorsStack.addLast(token);
                } else if (token.equals(")")) {
                    while (!tempOperatorsStack.peekLast().equals("(")) {
                        postfixExprStack.addLast(tempOperatorsStack.pollLast());
                        if (tempOperatorsStack.isEmpty())
                            return null;
                    }
                    tempOperatorsStack.pollLast();
                } else {
                    if (!tempOperatorsStack.isEmpty() && priority(token) <= priority(tempOperatorsStack.peekLast())) {
                        postfixExprStack.addLast(tempOperatorsStack.pollLast());
                        tempOperatorsStack.addLast(token);

                    } else tempOperatorsStack.addLast(token);
                }
            }

            if (tempOperatorsStack.contains("(")) {
                throw new CannotConvertIntoPostfixException();
            }

            while (!tempOperatorsStack.isEmpty())
                postfixExprStack.addLast(tempOperatorsStack.pollLast());

            return postfixExprStack;
        }

        //reverse transform method could be added in future if needed

        //returns an integer value of mathematical operators priority
        private static int priority(String token) {
            switch (token) {
                case "/":
                case "*":
                    return 2;
                case "(":
                case ")":
                    return -1;
                default:
                    return 1;
            }
        }
    }
}
//:~