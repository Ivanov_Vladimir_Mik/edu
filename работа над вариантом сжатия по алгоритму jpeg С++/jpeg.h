#ifndef JPEG
#define JPEG
#include <vector>

using namespace std;

const int n = 8, m = 8;
const double pi = 3.142857;

// bitmap file header
typedef struct {
	unsigned short bfType;
	unsigned int   bfSize;
	unsigned short bfReserved1;
	unsigned short bfReserved2;
	unsigned int   bfOffBits;
} BITMAPFILEHEADER;

// bitmap info header
typedef struct {
	unsigned int   biSize;
	unsigned int   biWidth;
	unsigned int   biHeight;
	unsigned short biPlanes;
	unsigned short biBitCount;
	unsigned int   biCompression;
	unsigned int   biSizeImage;
	unsigned int   biXPelsPerMeter;
	unsigned int   biYPelsPerMeter;
	unsigned int   biClrUsed;
	unsigned int   biClrImportant;
	unsigned int   biRedMask;
	unsigned int   biGreenMask;
	unsigned int   biBlueMask;
	unsigned int   biAlphaMask;
} BITMAPINFOHEADER;

// ��������� RGB
typedef struct {
	unsigned char  rgbBlue;
	unsigned char  rgbGreen;
	unsigned char  rgbRed;
	unsigned char  rgbReserved;
} RGBQUAD;

// ������ ������� 8�8
typedef struct 
{
	double chan[8][8];

	void out()
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				cout << chan[i][j] << "\t";
			}
			cout << endl;
		}
	}
} chanNode;

typedef struct {
	unsigned short bfType;
	unsigned short qRatio;
	unsigned int height;
	unsigned int width;
	unsigned int numOfBlocks;
	unsigned int sizeOfData;
	
} BINHEADER;
// ������ ��������
class Node
{
public:
	int count;
	int symbol;
	Node* left, *right;
	Node() {}
	Node(Node *L, Node *R)
	{
		left = L;
		right = R;
		count = L->count + R->count;
		symbol = INT_MAX;
	}
};

struct MyCompare
{
	bool operator()(Node* l, Node* r) const
	{
		return l->count < r->count;
	}
};

// ������ ���
template <typename Type>
void read(ifstream &fp, Type &result, size_t size) {
	fp.read(reinterpret_cast<char*>(&result), size);
}

template <typename Type>
void write(ofstream &fp, Type &data, size_t size) {
	fp.write(reinterpret_cast<char*>(&data), size);
}
// ���������� ���
unsigned char bitextract(const unsigned int byte, const unsigned int mask);

//  YCbCr �� RGB
void YUVfromRGB(RGBQUAD** rgb, double** Y, double** Cb, double** Cr, unsigned int height, unsigned int width);
//  RGB �� YCbCr
void RGBfromYUV(RGBQUAD** rgb, double** Y, double** Cb, double** Cr, unsigned int height, unsigned int width);

// ���������������� 
void sub422(double** chan, int height, int width);
void sub420(double** chan, int height, int width);

//��� ������ ��������������
void dctTransform(chanNode* matrix);
//��� �������� ��������������
void idctTransform(chanNode* matrix);

//������������ ������� � ������
void zigzagToList(vector<int> *chan, chanNode* matrix);
vector<int> zigzagToList(int m[n][m]);

//�������������� ������� � ������� �� �������
void zigzagToMatrix(vector<int> v, chanNode* m);
void zigzagFromList(vector<int> v, int table[n][m]);

//���������� ������ ��� ������ ��������
void makeDCmap(vector <vector <int> > &chan, map<int, int> &m);
void makeDCmap(vector <vector <int> > &chan1, vector <vector <int> > &chan2, map<int, int> & m);
void makeACmap(vector <vector <int> > &chan, map<int, int> &m);
void makeACmap(vector <vector <int> > &chan1, vector <vector <int> > &chan2, map<int, int> & m);

//������ �������� ����� ���
void compressChan(vector<vector <int> > &chan);

//���������� ������ ��������
Node* buildHaffmanTree(map<int, int> m);

//��������� ������ ��� ����������
void getHaffmanKeys(Node* root, map<int, vector<bool> > &table);

//����� ������
void printTree(Node* root, unsigned k = 0);

//������������ map � vector
vector<int> convert(map<int, int> m);

int menu();
#endif 