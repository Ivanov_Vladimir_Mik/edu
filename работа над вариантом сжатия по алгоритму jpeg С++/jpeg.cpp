#include <stdlib.h>

#include <string>
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include <map>
#include <list>


#include "jpeg.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	int userMenu;

	do {
		userMenu = menu();

		switch (userMenu)
		{
		case 0x31:
		{
			string fileName;
			int height = 0, width = 0;

			cout << "������� ��� �����" << endl;
			cin >> fileName;

			string resultFile;
			cout << "������� ��� �����, ���� ��������� ���������" << endl;
			cin >> resultFile;

			// ��������� ����
			ifstream fileStream(fileName, ifstream::binary);
			if (!fileStream) {
				cout << "Error opening file '" << fileName << "'." << endl;
				break;
			}

			//���� ��������

			int qRatio = 0;

			cout << "������� ����������� �����������, ��� ������ �������� - ��� ���� ��������: " << endl
				<< "�������� ����� ���� ������������� ����� ������" << endl;

			do {
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cin >> qRatio;
				while (cin.fail())
				{
					cin.clear();
					cin.ignore(cin.rdbuf()->in_avail());
					cout << "������! ������� ��������� ��������: " << endl;
					cin >> qRatio;
				}
				if (qRatio < 1)
					cout << "������! ������� �������� ��������: " << endl;
			} while (qRatio < 1);

			int subRatio = 0;

			cout << "����� ������� ������������� ��������� ���������: " << endl;
			cout << "1. 4:2:0 " << endl;
			cout << "2. 4:2:2 " << endl;
			cout << "3. 4:4:4 (��� ���������) " << endl;
			do {
				cin.clear();
				cin.ignore(cin.rdbuf()->in_avail());
				cin >> subRatio;

				if (subRatio < 1 || subRatio > 3)
				{
					cout << "������! ������� ��������� ��������: " << endl;
				}
				while (cin.fail())
				{
					cin.clear();
					cin.ignore(cin.rdbuf()->in_avail());
					cout << "������! ������� ��������� ��������: " << endl;
					cin >> subRatio;
				}
			} while (subRatio < 1 || subRatio > 3);

			cout << "������ �����������..." << endl;
			// �������� �����������
			BITMAPFILEHEADER fileHeader;
			read(fileStream, fileHeader.bfType, sizeof(fileHeader.bfType));
			read(fileStream, fileHeader.bfSize, sizeof(fileHeader.bfSize));
			read(fileStream, fileHeader.bfReserved1, sizeof(fileHeader.bfReserved1));
			read(fileStream, fileHeader.bfReserved2, sizeof(fileHeader.bfReserved2));
			read(fileStream, fileHeader.bfOffBits, sizeof(fileHeader.bfOffBits));

			if (fileHeader.bfType != 0x4D42) {
				cout << "Error: '" << fileName << "is not BMP file." << endl;
				break;
			}

			// ���������� �����������
			BITMAPINFOHEADER fileInfoHeader;
			read(fileStream, fileInfoHeader.biSize, sizeof(fileInfoHeader.biSize));

			// bmp core
			if (fileInfoHeader.biSize >= 12) {
				read(fileStream, fileInfoHeader.biWidth, sizeof(fileInfoHeader.biWidth));
				read(fileStream, fileInfoHeader.biHeight, sizeof(fileInfoHeader.biHeight));
				read(fileStream, fileInfoHeader.biPlanes, sizeof(fileInfoHeader.biPlanes));
				read(fileStream, fileInfoHeader.biBitCount, sizeof(fileInfoHeader.biBitCount));
			}

			// �������� ���������� � ��������
			int colorsCount = fileInfoHeader.biBitCount >> 3;
			if (colorsCount < 3) {
				colorsCount = 3;
			}

			int bitsOnColor = fileInfoHeader.biBitCount / colorsCount;
			int maskValue = (1 << bitsOnColor) - 1;

			if (fileInfoHeader.biSize >= 40) {
				read(fileStream, fileInfoHeader.biCompression, sizeof(fileInfoHeader.biCompression));
				read(fileStream, fileInfoHeader.biSizeImage, sizeof(fileInfoHeader.biSizeImage));
				read(fileStream, fileInfoHeader.biXPelsPerMeter, sizeof(fileInfoHeader.biXPelsPerMeter));
				read(fileStream, fileInfoHeader.biYPelsPerMeter, sizeof(fileInfoHeader.biYPelsPerMeter));
				read(fileStream, fileInfoHeader.biClrUsed, sizeof(fileInfoHeader.biClrUsed));
				read(fileStream, fileInfoHeader.biClrImportant, sizeof(fileInfoHeader.biClrImportant));
			}

			// ������ ����� �� ���������
			fileInfoHeader.biRedMask = maskValue << (bitsOnColor * 2);
			fileInfoHeader.biGreenMask = maskValue << bitsOnColor;
			fileInfoHeader.biBlueMask = maskValue;
			fileInfoHeader.biAlphaMask = maskValue << (bitsOnColor * 3);

			// �������� �� �������� ���� ������ �������
			if (fileInfoHeader.biSize != 40) {
				cout << "Error: Unsupported BMP format." << endl;
				break;
			}

			if (fileInfoHeader.biBitCount != 16 && fileInfoHeader.biBitCount != 24 && fileInfoHeader.biBitCount != 32) {
				cout << "Error: Unsupported BMP bit count." << endl;
				break;
			}

			if (fileInfoHeader.biCompression != 0 && fileInfoHeader.biCompression != 3) {
				cout << "Error: Unsupported BMP compression." << endl;
				break;
			}

			// rgb & YCbCr info
			RGBQUAD **rgbInfo = new RGBQUAD*[fileInfoHeader.biHeight];
			double** chanY = new double*[fileInfoHeader.biHeight];
			double** chanCr = new double*[fileInfoHeader.biHeight];
			double** chanCb = new double*[fileInfoHeader.biHeight];

			for (unsigned int i = 0; i < fileInfoHeader.biHeight; i++) {
				rgbInfo[i] = new RGBQUAD[fileInfoHeader.biWidth];
				chanY[i] = new double[fileInfoHeader.biWidth];
				chanCb[i] = new double[fileInfoHeader.biWidth];
				chanCr[i] = new double[fileInfoHeader.biWidth];
			}

			// ����������� ������� ������� � ����� ������ ������
			int linePadding = fileInfoHeader.biWidth * fileInfoHeader.biBitCount/8 % 4;
			if (linePadding != 0)
				linePadding = 4 - linePadding;

			// ������
			unsigned int bufer;

			for (unsigned int i = 0; i < fileInfoHeader.biHeight; i++) {
				for (unsigned int j = 0; j < fileInfoHeader.biWidth; j++) {
					//read(fileStream, bufer, fileInfoHeader.biBitCount / 8);

					read(fileStream, rgbInfo[i][j].rgbRed, sizeof(bool));
					read(fileStream, rgbInfo[i][j].rgbGreen, sizeof(bool));
					read(fileStream, rgbInfo[i][j].rgbBlue, sizeof(bool));

					//rgbInfo[i][j].rgbRed = bitextract(bufer, fileInfoHeader.biRedMask);
					//rgbInfo[i][j].rgbGreen = bitextract(bufer, fileInfoHeader.biGreenMask);
					//rgbInfo[i][j].rgbBlue = bitextract(bufer, fileInfoHeader.biBlueMask);
					//rgbInfo[i][j].rgbReserved = bitextract(bufer, fileInfoHeader.biAlphaMask);
				}
				fileStream.seekg(linePadding, ios_base::cur);
			}

			cout << "������� � �������� ������������ YCbCr..." << endl;
			//�������������� �������� �� RGB � YCbCr
			YUVfromRGB(rgbInfo, chanY, chanCb, chanCr, fileInfoHeader.biHeight, fileInfoHeader.biWidth);

			cout << "����������������..." << endl;
			//C��������������� ��������� ���������
			switch (subRatio)
			{
			case 1:
			{
				sub420(chanCb, fileInfoHeader.biHeight, fileInfoHeader.biWidth);
				break;
			}
			case 2:
			{
				sub422(chanCb, fileInfoHeader.biHeight, fileInfoHeader.biWidth);
				break;
			}
			case 3:
				break;

			default:
				break;
			}

			//������� ��������, �������� 8�8 ���������� ������� ��� � �� � �r.
			long int heightTemp = 0, widthTemp = 0;
			bool h = false, w = false;

			if (fileInfoHeader.biHeight % 8) h = true;
			if (fileInfoHeader.biWidth % 8) w = true;

			if (h)
			{
				heightTemp = fileInfoHeader.biHeight / 8 + 1;
			}
			else heightTemp = fileInfoHeader.biHeight / 8;

			if (w)
			{
				widthTemp = fileInfoHeader.biWidth / 8 + 1;
			}
			else widthTemp = fileInfoHeader.biWidth / 8;

			const long int numOfBlocks = heightTemp * widthTemp;

			//���������� ���� ��������.
			chanNode* chanY8_8 = new chanNode[numOfBlocks];
			chanNode* chanCb8_8 = new chanNode[numOfBlocks];
			chanNode* chanCr8_8 = new chanNode[numOfBlocks];

			cout << "��������� �� ������� 8�8..." << endl;
			//��������� ����������� 8�8
			int count = 0;
			int countX = 0;
			int l = 0;

			while (count < fileInfoHeader.biHeight)
			{
				while (countX < fileInfoHeader.biWidth)
				{
					for (int i = count; i < count + 8; i++)
					{
						for (int j = countX; j < countX + 8; j++)
						{
							if (i >= fileInfoHeader.biHeight || j >= fileInfoHeader.biWidth)
							{
								chanY8_8[l].chan[i - count][j - countX] = 0;
								chanCb8_8[l].chan[i - count][j - countX] = 0;
								chanCr8_8[l].chan[i - count][j - countX] = 0;
							}
							else
							{
								chanY8_8[l].chan[i - count][j - countX] = chanY[i][j];
								chanCb8_8[l].chan[i - count][j - countX] = chanCb[i][j];
								chanCr8_8[l].chan[i - count][j - countX] = chanCr[i][j];
							}
						}
					}
					l++;
					countX += 8;
				}
				count += 8;
				countX = 0;
			}

			/* //����� �� ����� ����������� ������������ ��� ���������� ������ 8�8
			for (int k =0; k < 4; k++)
				for (int i = 0; i < 8; i++)
				{
					for (int j = 0; j < 8; j++)
					{
						cout << chanCb8_8[k].chan[i][j] << " ";
					}
					cout << endl;
				}
			*/

			for (unsigned int i = 0; i < fileInfoHeader.biHeight; i++) {
				delete rgbInfo[i];
				delete chanY[i];
				delete chanCb[i];
				delete chanCr[i];
			}
			delete rgbInfo;
			delete chanY;
			delete chanCr;
			delete chanCb;

			cout << "���..." << endl;
			//������ ���
			for (int i = 0; i < numOfBlocks; i++)
			{
				dctTransform(&chanY8_8[i]);
				dctTransform(&chanCb8_8[i]);
				dctTransform(&chanCr8_8[i]);
			}

			cout << "�����������..." << endl;
			//�����������
			int qt[m][n] = {
				{ 3,  2,  2,  3,  5,  8,  10, 12 },
				{ 2,  2,  3,  4,  5,  12, 12, 11 },
				{ 3,  3,  3,  5,  8,  11, 14, 11 },
				{ 3,  3,  4,  6,  10, 17, 16, 12 },
				{ 4,  4,  7,  11, 14, 22, 21, 15 },
				{ 5,  7,  11, 13, 16, 21, 23, 18 },
				{ 10, 13, 16, 17, 21, 24, 24, 20 },
				{ 14, 18, 19, 20, 22, 20, 21, 20 }
			};

			for (int k = 0; k < numOfBlocks; k++)
			{
				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < m; j++)
					{
						chanY8_8[k].chan[i][j] = static_cast<int>(chanY8_8[k].chan[i][j] / (qt[i][j] * qRatio));
						chanCb8_8[k].chan[i][j] = static_cast<int>(chanCb8_8[k].chan[i][j] / (qt[i][j] * qRatio));
						chanCr8_8[k].chan[i][j] = static_cast<int>(chanCr8_8[k].chan[i][j] / (qt[i][j] * qRatio));
					}
				}
			}

			cout << "����� ��������..." << endl;

			//����� ��������

			vector <vector<int> > Y(numOfBlocks);
			vector <vector<int> > Cb(numOfBlocks);
			vector <vector<int> > Cr(numOfBlocks);

			for (int i = 0; i < numOfBlocks; i++)
			{
				zigzagToList(&Y[i], &chanY8_8[i]);
				zigzagToList(&Cb[i], &chanCb8_8[i]);
				zigzagToList(&Cr[i], &chanCr8_8[i]);
			}

			delete chanY8_8;
			delete chanCb8_8;
			delete chanCr8_8;

			//������������� ����� �� ������
			/*cout << "�� ������" << endl;

			int kk = 0;

			for (int i = 0; i < numOfBlocks; i++)
			{
				for (int j = 0; j < Y[i].size(); j++)
				{
					cout << Y[i][j] << " ";
					kk++;
				}
				cout << endl;
			}
			cout << kk << endl << endl;

			kk = 0;

			for (int i = 0; i < numOfBlocks; i++)
			{
				for (int j = 0; j < Cb[i].size(); j++)
				{
					cout << Cb[i][j] << " ";
					kk++;
				}
				cout << endl;
			}
			cout << kk << endl << endl;

			kk = 0;

			for (int i = 0; i < numOfBlocks; i++)
			{
				for (int j = 0; j < Cr[i].size(); j++)
				{
					cout << Cr[i][j] << " ";
					kk++;
				}
				cout << endl;
			}
			cout << kk << endl << endl;
			*/

			//������ � ����
			compressChan(Y);
			compressChan(Cb);
			compressChan(Cr);

			/*
			//������������� ����� ����� ������
			kk = 0;

			for (int i = 0; i < numOfBlocks; i++)
			{
				for (int j = 0; j < Y[i].size(); j++)
				{
					cout << Y[i][j] << " ";
					kk++;
				}
				cout << endl;
			}
			cout << kk << endl;
			*/

			cout << "����������� ��������..." << endl;
			//����������� ��������
			map<int, int> YDCmap;
			map<int, int> YACmap;
			map<int, int> CbCrDCmap;
			map<int, int> CbCrACmap;

			map<int, int>::iterator iter;
			int maxLength = 0;
			//�������� ������� ����������� ������ � �������� �� ���������
			makeDCmap(Y, YDCmap);
			makeDCmap(Cb, Cr, CbCrDCmap);
			makeACmap(Y, YACmap);
			makeACmap(Cb, Cr, CbCrACmap);

			/*
			//�������� ����� ��� DC
			cout << "YDC" << endl;
			for (it = YDCmap.begin(); it != YDCmap.end(); it++)
				cout << it->first << " : " << it->second << endl;

			//�������� ����� ��� AC
			cout << "YAC" << endl;
			for (it = YACmap.begin(); it != YACmap.end(); it++)
				cout << it->first << " : " << it->second << endl;
			*/

			Node* root = buildHaffmanTree(YDCmap);
			map<int, vector<bool> > YDCkeys;
			getHaffmanKeys(root, YDCkeys);

			map<int, vector<bool> > YACkeys;
			root = buildHaffmanTree(YACmap);
			getHaffmanKeys(root, YACkeys);

			map<int, vector<bool> > CbCrDCkeys;
			root = buildHaffmanTree(CbCrDCmap);
			getHaffmanKeys(root, CbCrDCkeys);

			map<int, vector<bool> > CbCrACkeys;
			root = buildHaffmanTree(CbCrACmap);
			getHaffmanKeys(root, CbCrACkeys);

			cout << "������ � ����..." << endl;

			//������ ����� jpgv

			//��������� �����
			BINHEADER header;
			header.bfType = 43775;
			header.qRatio = qRatio;
			header.height = fileInfoHeader.biHeight;
			header.width = fileInfoHeader.biWidth;
			header.numOfBlocks = numOfBlocks;
			header.sizeOfData = 0;

			{
				ofstream result(resultFile, ios::binary);

				//��������� �����
				write(result, header, sizeof(header));
				//result.write((char*)&header, sizeof(header));

				//������� �����������
				vector<int> qTab = zigzagToList(qt);
				//vector<int>::iterator it;
				for (auto & it : qTab)
				{
					write(result, it, sizeof(it));
				}

				//write(result, qTab, sizeof(qTab));

				qTab.clear();

				//���� �������� ��� YDC

				int mapSize = 0;

				vector<int> temp3;
				temp3 = convert(YDCmap);
				mapSize = temp3.size();

				write(result, mapSize, sizeof(mapSize));

				for (auto &i : temp3)
				{
					write(result, i, sizeof(i));
				}

				//���� �������� ��� YAC

				temp3 = convert(YACmap);
				mapSize = temp3.size();

				write(result, mapSize, sizeof(mapSize));

				for (auto &i : temp3)
				{
					write(result, i, sizeof(i));
				}

				//���� �������� ��� CbCrDC
				temp3 = convert(CbCrDCmap);
				mapSize = temp3.size();

				write(result, mapSize, sizeof(mapSize));

				for (auto &i : temp3)
				{
					write(result, i, sizeof(i));
				}

				//���� �������� ��� CbCrAC
				temp3 = convert(CbCrACmap);
				mapSize = temp3.size();

				write(result, mapSize, sizeof(mapSize));

				for (auto &i : temp3)
				{
					write(result, i, sizeof(i));
				}

				//������������� ������
				int hCount = 0; unsigned char buf = 0;

				long int encodedSize = 0;
				int prevYDC = 0, prevCbDC = 0, prevCrDC = 0;

				for (int i = 0; i < numOfBlocks; i++)
				{
					unsigned char point = Y[i].size();
					write(result, point, sizeof(point));

					point = Cb[i].size();
					write(result, point, sizeof(point));

					point = Cr[i].size();
					write(result, point, sizeof(point));

					if (i == 11637)
					{
						i = 11637;
					}

					for (int y = 0; y < Y[i].size(); y++)
					{
						vector<bool> yx;

						if (y == 0)
						{
							int t = Y[i][y];
							int c = Y[i][y] - prevYDC;
							prevYDC = t;
							yx = YDCkeys[c];
						}
						else
						{
							int c = Y[i][y];
							yx = YACkeys[c];
						}

						for (int n = 0; n < yx.size(); n++)
						{
							buf = buf | yx[n] << (7 - hCount);
							hCount++;
							if (hCount == 8)
							{
								hCount = 0;
								write(result, buf, sizeof(buf));
								buf = 0;
								header.sizeOfData++;
							}
						}
					}

					for (int cb = 0; cb < Cb[i].size(); cb++)
					{
						vector<bool> cbx;

						if (cb == 0)
						{
							int t = Cb[i][cb];
							int c = Cb[i][cb] - prevCbDC;
							prevCbDC = t;
							cbx = CbCrDCkeys[c];
						}
						else
						{
							int c = Cb[i][cb];
							cbx = CbCrACkeys[c];
						}

						for (int n = 0; n < cbx.size(); n++)
						{
							buf = buf | cbx[n] << (7 - hCount);
							hCount++;
							if (hCount == 8)
							{
								hCount = 0;
								write(result, buf, sizeof(buf));
								buf = 0;
								header.sizeOfData++;
							}
						}
					}

					for (int cr = 0; cr < Cr[i].size(); cr++)
					{
						vector<bool> crx;

						if (cr == 0)
						{
							int t = Cr[i][cr];
							int c = Cr[i][cr] - prevCrDC;
							prevCrDC = t;
							crx = CbCrDCkeys[c];
						}
						else
						{
							int c = Cr[i][cr];
							crx = CbCrACkeys[c];
						}

						for (int n = 0; n < crx.size(); n++)
						{
							buf = buf | crx[n] << (7 - hCount);
							hCount++;
							if (hCount == 8)
							{
								hCount = 0;
								write(result, buf, sizeof(buf));
								buf = 0;
								header.sizeOfData++;
							}
						}
					}

					write(result, buf, sizeof(buf));

				}

				//����� ������������� ��������
				unsigned short end = 65279;
				write(result, end, sizeof(end));

				result.seekp(16, ios::beg);
				write(result, header.sizeOfData, sizeof(header.sizeOfData));

				result.close();
			}

			header = {};
			fileHeader = {};
			fileInfoHeader = {};
			YDCkeys.clear();
			YACkeys.clear();
			CbCrDCkeys.clear();
			CbCrACkeys.clear();
			YDCmap.clear();
			YACmap.clear();
			CbCrDCmap.clear();
			CbCrACmap.clear();
			Y.clear();
			Cb.clear();
			Cr.clear();

			cout << "���� �������! " << endl;
			break;
		}
		case 0x32:
		{
			//������ ����� bin



			string fileName;
			cout << "������� ��� �����" << endl;
			cin >> fileName;

			string resultFile;
			cout << "������� ��� �����, ���� ��������� ���������" << endl;
			cin >> resultFile;

			BINHEADER binHeader = {};
			vector<int> qTab;

			// ��������� ����

			ifstream fileStream(fileName, ios::binary);
			if (!fileStream) {
				cout << "Error opening file '" << fileName << "'." << endl;
				break;
			}


			cout << "������ �����������..." << endl;

			//��������� �����

			read(fileStream, binHeader, sizeof(binHeader));

			if (binHeader.bfType != 43775) { cout << "�������� ������ �����!!! " << endl; break; }


			//������� �����������
			int temp4 = 0;
			for (int i = 0; i < 64; i++)
			{
				read(fileStream, temp4, sizeof(temp4));
				qTab.push_back(temp4);
			}

			map<int, int> YDCmap;
			map<int, int> YACmap;
			map<int, int> CbCrDCmap;
			map<int, int> CbCrACmap;

			Node* YDCtree;
			Node* YACtree;
			Node* CbCrDCtree;
			Node* CbCrACtree;

			int mapSize = 0;

			//���� �������� ��� YDC
			int x = 0, y = 0;

			read(fileStream, mapSize, sizeof(mapSize));
			for (int i = 0; i < mapSize / 2; i++)
			{

				read(fileStream, x, sizeof(x));
				read(fileStream, y, sizeof(y));
				YDCmap[x] = y;

			}

			YDCtree = buildHaffmanTree(YDCmap);

			//���� �������� ��� YAC

			read(fileStream, mapSize, sizeof(mapSize));
			for (int i = 0; i < mapSize / 2; i++)
			{

				read(fileStream, x, sizeof(x));
				read(fileStream, y, sizeof(y));
				YACmap[x] = y;

			}

			YACtree = buildHaffmanTree(YACmap);

			//���� �������� ��� CbCrDC

			read(fileStream, mapSize, sizeof(mapSize));
			for (int i = 0; i < mapSize / 2; i++)
			{

				read(fileStream, x, sizeof(x));
				read(fileStream, y, sizeof(y));
				CbCrDCmap[x] = y;

			}

			CbCrDCtree = buildHaffmanTree(CbCrDCmap);

			//���� �������� ��� CbCrAC

			read(fileStream, mapSize, sizeof(mapSize));
			for (int i = 0; i < mapSize / 2; i++)
			{

				read(fileStream, x, sizeof(x));
				read(fileStream, y, sizeof(y));
				CbCrACmap[x] = y;

			}

			CbCrACtree = buildHaffmanTree(CbCrACmap);

			vector <vector<int> > Y(binHeader.numOfBlocks);
			vector <vector<int> > Cb(binHeader.numOfBlocks);
			vector <vector<int> > Cr(binHeader.numOfBlocks);

			//������ ������������� ������

			int offset = 0, Yprev = 0, Cbprev = 0, Crprev = 0;
			unsigned char buffer;
			unsigned char Ysize = 0, Cbsize = 0, Crsize = 0;


			for (int i = 0; i < binHeader.numOfBlocks; i++)
			{
				if (i == 30000)
				{
					i = 30000;
				}

				read(fileStream, Ysize, sizeof(Ysize));
				read(fileStream, Cbsize, sizeof(Cbsize));
				read(fileStream, Crsize, sizeof(Crsize));

				read(fileStream, buffer, sizeof(buffer));

				Node* root = YDCtree;

				for (int j = 0; j < Ysize; )
				{
					if (root->symbol != INT_MAX)
					{
						if (Y[i].size() == 0)
						{
							int tmp = root->symbol + Yprev;
							Y[i].push_back(tmp);
							Yprev = tmp;
							j++;
						}

						else
						{
							Y[i].push_back(root->symbol);
							j++;


							if (Y[i].back() == 0)
							{
								Y[i].pop_back();
								int tmp = Y[i].back();
								Y[i].pop_back();
								for (int y = 0; y < tmp; y++)
									Y[i].push_back(0);
							}
						}
						root = YACtree;
					}

					else if ((buffer >> (7 - offset) & 1) == 0)
					{
						root = root->left;
						offset++;
					}

					else
					{
						root = root->right;
						offset++;
					}

					if (offset == 8)
					{
						offset = 0; read(fileStream, buffer, sizeof(buffer));
					}
				}

				root = CbCrDCtree;

				for (int j = 0; j < Cbsize; )
				{
					if (root->symbol != INT_MAX)
					{
						if (Cb[i].size() == 0)
						{
							int tmp = root->symbol + Cbprev;
							Cb[i].push_back(tmp);
							Cbprev = tmp;
							j++;
						}

						else
						{
							Cb[i].push_back(root->symbol);
							j++;


							if (Cb[i].back() == 0)
							{
								Cb[i].pop_back();
								int tmp = Cb[i].back();
								Cb[i].pop_back();
								for (int y = 0; y < tmp; y++)
									Cb[i].push_back(0);
							}
						}

						root = CbCrACtree;
					}

					else if ((buffer >> (7 - offset) & 1) == 0)
					{
						root = root->left;
						offset++;
					}

					else
					{
						root = root->right;
						offset++;
					}

					if (offset == 8)
					{
						offset = 0; read(fileStream, buffer, sizeof(buffer));
					}
				}

				root = CbCrDCtree;

				for (int j = 0; j < Crsize; )
				{
					if (root->symbol != INT_MAX)
					{
						if (Cr[i].size() == 0)
						{
							int tmp = root->symbol + Crprev;
							Cr[i].push_back(tmp);
							Crprev = tmp;
							j++;
						}

						else
						{
							Cr[i].push_back(root->symbol);
							j++;


							if (Cr[i].back() == 0)
							{
								Cr[i].pop_back();
								int tmp = Cr[i].back();
								Cr[i].pop_back();
								for (int y = 0; y < tmp; y++)
									Cr[i].push_back(0);
							}
						}

						root = CbCrACtree;
					}

					else if ((buffer >> (7 - offset) & 1) == 0)
					{
						root = root->left;
						offset++;
					}

					else
					{
						root = root->right;
						offset++;
					}

					if (offset == 8)
					{
						offset = 0; read(fileStream, buffer, sizeof(buffer));
					}
				}

				if (Y[i].size() > 64 || Cb[i].size() > 64 || Cr[i].size() > 64)
				{
					cout << "���� ���������!!" << endl;
					break;
				}
			}

			unsigned short endPointer;
			read(fileStream, endPointer, sizeof(endPointer));

			if (endPointer != 65279)
			{
				cout << "���� ���������!!" << endl;
				break;
			}
			else
				cout << "������ ������ �������! " << endl;

			fileStream.close();
			//������������� ����� ����� ������
			/*
			{
				cout << "����� ������" << endl;
				int kk = 0;

				for (int i = 0; i < binHeader.numOfBlocks; i++)
				{
					for (int j = 0; j < Y[i].size(); j++)
					{
						cout << Y[i][j] << " ";
						kk++;
					}
					cout << endl;
				}
				cout << kk << endl << endl;

				kk = 0;

				for (int i = 0; i < binHeader.numOfBlocks; i++)
				{
					for (int j = 0; j < Cb[i].size(); j++)
					{
						cout << Cb[i][j] << " ";
						kk++;
					}
					cout << endl;
				}
				cout << kk << endl << endl;

				kk = 0;

				for (int i = 0; i < binHeader.numOfBlocks; i++)
				{
					for (int j = 0; j < Cr[i].size(); j++)
					{
						cout << Cr[i][j] << " ";
						kk++;
					}
					cout << endl;
				}
				cout << kk << endl << endl;
			}
			*/

			//�������� ������

			//������� ��������, �������� 8�8 ���������� ������� ��� � �� � �r.

			//���������� ���� ��������.
			chanNode* chanY8_8 = new chanNode[binHeader.numOfBlocks];
			chanNode* chanCb8_8 = new chanNode[binHeader.numOfBlocks];
			chanNode* chanCr8_8 = new chanNode[binHeader.numOfBlocks];

			for (int i = 0; i < binHeader.numOfBlocks; i++)
			{
				zigzagToMatrix(Y[i], &chanY8_8[i]);
				zigzagToMatrix(Cb[i], &chanCb8_8[i]);
				zigzagToMatrix(Cr[i], &chanCr8_8[i]);
			}

			//�������� �����������

			int qTable[8][8];

			zigzagFromList(qTab, qTable);

			for (int k = 0; k < binHeader.numOfBlocks; k++)
			{
				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < m; j++)
					{
						chanY8_8[k].chan[i][j] = chanY8_8[k].chan[i][j] * (qTable[i][j] * binHeader.qRatio);
						chanCb8_8[k].chan[i][j] = chanCb8_8[k].chan[i][j] * (qTable[i][j] * binHeader.qRatio);
						chanCr8_8[k].chan[i][j] = chanCr8_8[k].chan[i][j] * (qTable[i][j] * binHeader.qRatio);
					}
				}
			}

			//�������� ���

			for (int i = 0; i < binHeader.numOfBlocks; i++)
			{
				idctTransform(&chanY8_8[i]);
				idctTransform(&chanCb8_8[i]);
				idctTransform(&chanCr8_8[i]);
			}

			//��������� ����������� � YCbCr

			long int heightTemp = 0, widthTemp = 0;
			bool h = false, w = false;

			if (binHeader.height % 8) h = true;
			if (binHeader.width % 8) w = true;

			if (h)
			{
				heightTemp = binHeader.height / 8 + 1;
			}
			else heightTemp = binHeader.height / 8;

			if (w)
			{
				widthTemp = binHeader.width / 8 + 1;
			}
			else widthTemp = binHeader.width / 8;

			heightTemp *= 8;
			widthTemp *= 8;

			RGBQUAD **rgbInfo = new RGBQUAD*[binHeader.height];
			double** chanY = new double*[heightTemp];
			double** chanCr = new double*[heightTemp];
			double** chanCb = new double*[heightTemp];

			for (unsigned int i = 0; i < binHeader.height; i++) {
				rgbInfo[i] = new RGBQUAD[binHeader.width];
			}
			for (unsigned int i = 0; i < heightTemp; i++) {
				chanY[i] = new double[widthTemp];
				chanCb[i] = new double[widthTemp];
				chanCr[i] = new double[widthTemp];
			}


			int hCount = 0, wCount = 0, l = 0;

			while (hCount < (heightTemp))
			{
				while (wCount < widthTemp)
				{
					for (int i = hCount; i < hCount + 8; i++)
					{
						for (int j = wCount; j < wCount + 8; j++)
						{

							chanY[i][j] = chanY8_8[l].chan[i - hCount][j - wCount];
							chanCb[i][j] = chanCb8_8[l].chan[i - hCount][j - wCount];
							chanCr[i][j] = chanCr8_8[l].chan[i - hCount][j - wCount];

						}
					}
					l++;
					wCount += 8;
				}
				hCount += 8;
				wCount = 0;
			}

			//�������������� � RGB

			RGBfromYUV(rgbInfo, chanY, chanCb, chanCr, binHeader.height, binHeader.width);

			//������ ����� bmp
			cout << "������ �����������..." << endl;

			ofstream result(resultFile, ios::binary);


			// �������� �����������
			BITMAPFILEHEADER fileHeader;
			fileHeader.bfType = 0x4d42;
			fileHeader.bfSize = binHeader.height * binHeader.width * 3 + 54;
			fileHeader.bfOffBits = 54;
			fileHeader.bfReserved1 = 0;
			fileHeader.bfReserved2 = 0;


			write(result, fileHeader.bfType, sizeof(fileHeader.bfType));
			write(result, fileHeader.bfSize, sizeof(fileHeader.bfSize));
			write(result, fileHeader.bfReserved1, sizeof(fileHeader.bfReserved1));
			write(result, fileHeader.bfReserved2, sizeof(fileHeader.bfReserved2));
			write(result, fileHeader.bfOffBits, sizeof(fileHeader.bfOffBits));

			// ���������� �����������
			BITMAPINFOHEADER fileInfoHeader;

			fileInfoHeader.biSize = 40;
			fileInfoHeader.biWidth = binHeader.width;
			fileInfoHeader.biHeight = binHeader.height;
			fileInfoHeader.biPlanes = 1;
			fileInfoHeader.biBitCount = 24;
			fileInfoHeader.biCompression = 0;
			fileInfoHeader.biSizeImage = binHeader.height * binHeader.width * 3;
			fileInfoHeader.biXPelsPerMeter = 3780;
			fileInfoHeader.biYPelsPerMeter = 3780;
			fileInfoHeader.biClrUsed = 0;
			fileInfoHeader.biClrImportant = 0;

			write(result, fileInfoHeader.biSize, sizeof(fileInfoHeader.biSize));
			write(result, fileInfoHeader.biWidth, sizeof(fileInfoHeader.biWidth));
			write(result, fileInfoHeader.biHeight, sizeof(fileInfoHeader.biHeight));
			write(result, fileInfoHeader.biPlanes, sizeof(fileInfoHeader.biPlanes));
			write(result, fileInfoHeader.biBitCount, sizeof(fileInfoHeader.biBitCount));

			int bitsOnColor = fileInfoHeader.biBitCount / 3;
			int maskValue = (1 << bitsOnColor) - 1;

			write(result, fileInfoHeader.biCompression, sizeof(fileInfoHeader.biCompression));
			write(result, fileInfoHeader.biSizeImage, sizeof(fileInfoHeader.biSizeImage));
			write(result, fileInfoHeader.biXPelsPerMeter, sizeof(fileInfoHeader.biXPelsPerMeter));
			write(result, fileInfoHeader.biYPelsPerMeter, sizeof(fileInfoHeader.biYPelsPerMeter));
			write(result, fileInfoHeader.biClrUsed, sizeof(fileInfoHeader.biClrUsed));
			write(result, fileInfoHeader.biClrImportant, sizeof(fileInfoHeader.biClrImportant));


			// ������ ����� �� ���������
			fileInfoHeader.biRedMask = maskValue << (bitsOnColor * 2);
			fileInfoHeader.biGreenMask = maskValue << bitsOnColor;
			fileInfoHeader.biBlueMask = maskValue;
			fileInfoHeader.biAlphaMask = maskValue << (bitsOnColor * 3);

			// ����������� ������� ������� � ����� ������ ������
			int linePadding = ((fileInfoHeader.biWidth * (fileInfoHeader.biBitCount / 8)) % 4) & 3;

			// ������
			unsigned char bufer;

			for (unsigned int i = 0; i < fileInfoHeader.biHeight; i++) {
				for (unsigned int j = 0; j < fileInfoHeader.biWidth; j++) {
					bufer = rgbInfo[i][j].rgbRed;
					write(result, bufer, sizeof(bufer));
					bufer = rgbInfo[i][j].rgbGreen;
					write(result, bufer, sizeof(bufer));
					bufer = rgbInfo[i][j].rgbBlue;
					write(result, bufer, sizeof(bufer));
				}
				fileStream.seekg(linePadding, ios_base::cur);
			}
			unsigned short ending = 0xf9f8;
			write(result, ending, sizeof(ending));

			break;
		}

		default:
		{
			break;
		}

		}
	} while (userMenu != 0x33);

	return 1;
}

//���������� ������ ��������

Node* buildHaffmanTree(map<int, int> m)
{
	list<Node*> hTree;

	map<int, int>::iterator it;

	for (it = m.begin(); it != m.end(); it++)
	{
		Node *p = new Node;
		p->symbol = it->first;
		p->count = it->second;
		hTree.push_back(p);
	}

	while (hTree.size() != 1)
	{
		hTree.sort(MyCompare());

		Node* SonL = hTree.front();
		hTree.pop_front();
		Node* SonR = hTree.front();
		hTree.pop_front();

		Node* parent = new Node(SonL, SonR);
		hTree.push_back(parent);
	}

	Node* root = hTree.front();
	return root;
}

//��������� ������ ������
vector <bool> key;

void getHaffmanKeys(Node* root, map<int, vector<bool> > &table)
{
	if (root->left != NULL)
	{
		key.push_back(0);
		getHaffmanKeys(root->left, table);
	}

	if (root->right != NULL)
	{
		key.push_back(1);
		getHaffmanKeys(root->right, table);
	}

	if (root->symbol != INT_MAX)
	{
		table[root->symbol] = key;
	}
	if (key.size() != 0)
		key.pop_back();
}
//������� ���������� ������� ��������� DC �������������
void makeDCmap(vector <vector <int> > &chan, map<int, int> &m)
{
	int dDC = 0;
	int dcCur = 0, dcPrev = 0;

	for (int i = 0; i < chan.size(); i++)
	{
		dcCur = chan[i][0];

		dDC = dcCur - dcPrev;
		dcPrev = dcCur;

		m[dDC]++;
	}
}

void makeDCmap(vector <vector <int> > &chan1, vector <vector <int> > &chan2, map<int, int> & m)
{
	int dDC = 0;
	int dcCur = 0, dcPrev = 0;

	for (int i = 0; i < chan1.size(); i++)
	{
		dcCur = chan1[i][0];

		dDC = dcCur - dcPrev;
		dcPrev = dcCur;

		m[dDC]++;
	}

	dDC = 0, dcPrev = 0;

	for (int i = 0; i < chan2.size(); i++)
	{
		dcCur = chan2[i][0];

		dDC = dcCur - dcPrev;
		dcPrev = dcCur;

		m[dDC]++;
	}
}

//������� ���������� ������� AC �������������
void makeACmap(vector <vector <int> > &chan, map<int, int> &m)
{
	int cur = 0;
	for (int i = 0; i < chan.size(); i++)
	{
		for (int j = 1; j < chan[i].size(); j++)
		{
			cur = chan[i][j];
			m[cur]++;
		}
	}
}

void makeACmap(vector <vector <int> > &chan1, vector <vector <int> > &chan2, map<int, int> & m)
{
	int cur = 0;
	for (int i = 0; i < chan1.size(); i++)
	{
		for (int j = 1; j < chan1[i].size(); j++)
		{
			cur = chan1[i][j];
			m[cur]++;
		}
	}

	for (int i = 0; i < chan2.size(); i++)
	{
		for (int j = 0; j < chan2[i].size(); j++)
		{
			cur = chan2[i][j];
			m[cur]++;
		}
	}
}

//������ �����
void compressChan(vector<vector <int> > &chan)
{
	vector <int> temp1;
	int counter = 0;
	for (int i = 0; i < chan.size(); i++)
	{
		temp1.push_back(chan[i][0]);
		for (int j = 1; j < chan[i].size(); j++)
		{
			if (chan[i][j] == 0 && j != 63)
			{
				counter++;
			}
			else
			{
				if (j == 63)
				{
					counter++;
				}
				if (counter > 0)
				{
					temp1.push_back(counter);
					temp1.push_back(0);
					if (j != 63) temp1.push_back(chan[i][j]);
					counter = 0;
				}
				else temp1.push_back(chan[i][j]);
			}
		}
		temp1.swap(chan[i]);
		temp1.clear();
	}
}

//������� ������� �� �������
vector<int> zigzagToList(int m[n][m])
{
	enum toUpDown { NO = 0, UP, DOWN };

	toUpDown move = NO;
	vector<int> v;
	int i = 0, j = 0;
	v.push_back(m[i][j]);
	while (!(i == n - 1 && j == n - 1))
	{
		if (!move)
		{
			if (j == n - 1)
			{
				move = DOWN;
				i++;
			}
			else if (i == n - 1)
			{
				move = UP;
				j++;
			}
			else if (!i)
			{
				move = DOWN;
				j++;
			}
			else if (!j)
			{
				move = UP;
				i++;
			}
		}
		else
		{
			if (move == UP)
			{
				i--;
				j++;
			}
			else if (move == DOWN)
			{
				i++;
				j--;
			}
			if (!i || !j || i == n - 1 || j == n - 1)
			{
				move = NO;
			}
		}
		v.push_back(m[i][j]);
	}
	return v;
}

void zigzagToList(vector<int> *chan, chanNode* matrix)
{
	enum toUpDown { NO = 0, UP, DOWN };

	toUpDown move = NO;

	int i = 0, j = 0;
	chan->push_back(matrix->chan[i][j]);
	while (!(i == n - 1 && j == n - 1))
	{
		if (!move)
		{
			if (j == n - 1)
			{
				move = DOWN;
				i++;
			}
			else if (i == n - 1)
			{
				move = UP;
				j++;
			}
			else if (!i)
			{
				move = DOWN;
				j++;
			}
			else if (!j)
			{
				move = UP;
				i++;
			}
		}
		else
		{
			if (move == UP)
			{
				i--;
				j++;
			}
			else if (move == DOWN)
			{
				i++;
				j--;
			}
			if (!i || !j || i == n - 1 || j == n - 1)
			{
				move = NO;
			}
		}
		chan->push_back(matrix->chan[i][j]);
	}
}

void zigzagToMatrix(vector<int> v, chanNode* m)
{
	enum toUpDown { NO = 0, UP, DOWN };

	toUpDown move = NO;

	int i = 0, j = 0, q = 0;

	m->chan[i][j] = v.front();
	v.erase(v.begin());
	vector<int>(v).swap(v);

	while (!(i == n - 1 && j == n - 1))
	{
		if (!move)
		{
			if (j == n - 1)
			{
				move = DOWN;
				i++;
			}
			else if (i == n - 1)
			{
				move = UP;
				j++;
			}
			else if (!i)
			{
				move = DOWN;
				j++;
			}
			else if (!j)
			{
				move = UP;
				i++;
			}
		}
		else
		{
			if (move == UP)
			{
				i--;
				j++;
			}
			else if (move == DOWN)
			{
				i++;
				j--;
			}
			if (!i || !j || i == n - 1 || j == n - 1)
			{
				move = NO;
			}
		}
		m->chan[i][j] = v.front();
		v.erase(v.begin());
		vector<int>(v).swap(v);
	}
}

void zigzagFromList(vector<int> v, int table[n][m])
{
	enum toUpDown { NO = 0, UP, DOWN };

	toUpDown move = NO;

	int i = 0, j = 0, q = 0;

	table[i][j] = v.front();
	v.erase(v.begin());
	vector<int>(v).swap(v);

	while (!(i == n - 1 && j == n - 1))
	{
		if (!move)
		{
			if (j == n - 1)
			{
				move = DOWN;
				i++;
			}
			else if (i == n - 1)
			{
				move = UP;
				j++;
			}
			else if (!i)
			{
				move = DOWN;
				j++;
			}
			else if (!j)
			{
				move = UP;
				i++;
			}
		}
		else
		{
			if (move == UP)
			{
				i--;
				j++;
			}
			else if (move == DOWN)
			{
				i++;
				j--;
			}
			if (!i || !j || i == n - 1 || j == n - 1)
			{
				move = NO;
			}
		}
		table[i][j] = v.front();
		v.erase(v.begin());
		vector<int>(v).swap(v);
	}
}
//������� ���
void dctTransform(chanNode* matrix)
{
	int i, j, u, v;

	double Cu, Cv, dct, sum;

	double dctMatrix[8][8], tmpm;

	for (u = 0; u < 8; u++) {
		for (v = 0; v < 8; v++) {

			if (u == 0) {
				Cu = 1.0 / sqrt(8.0);
			}
			else {
				Cu = (0.5);
			}

			if (v == 0) {
				Cv = 1.0 / sqrt(8.0);
			}
			else {
				Cv = (0.5);
			}

			sum = 0.0;

			for (i = 0; i < 8; i++) {
				for (j = 0; j < 8; j++) {

					tmpm = matrix->chan[i][j];

					dct = tmpm * cos((2 * i + 1) * u * pi / 16.0) *
						cos((2 * j + 1) * v * pi / 16.0);

					sum += dct;

				}
			}
			dctMatrix[u][v] = Cu * Cv * sum;
		}
	}
	for (int ii = 0; ii < 8; ii++)
		for (int jj = 0; jj < 8; jj++)
			matrix->chan[ii][jj] = dctMatrix[ii][jj];
}

//������� ��������� ���
void idctTransform(chanNode* matrix)
{
	double idct,
		Cu,
		sum,
		Cv;

	int i,
		j,
		u,
		v;

	float idctMatrix[8][8],
		tmpim;


	for (i = 0; i < 8; ++i) {
		for (j = 0; j < 8; ++j) {

			sum = 0.0;

			for (u = 0; u < 8; u++) {
				for (v = 0; v < 8; v++) {

					if (u == 0) {
						Cu = 1.0 / sqrt(8.0);
					}
					else {
						Cu = (0.5);
					}

					if (v == 0) {
						Cv = 1.0 / sqrt(8.0);
					}
					else {
						Cv = (0.5); //mistake was here - the same is in dct()
					}
					tmpim = matrix->chan[u][v];

					// Multiply by Cv and Cu here!
					idct = (tmpim * Cu * Cv *
						cos((2 * i + 1) * u * pi / 16.0) *
						cos((2 * j + 1) * v * pi / 16.0));

					sum += idct;
				}
			}
			// not "* Cv * Cu" here!
			idctMatrix[i][j] = sum;
		}
	}
	for (int ii = 0; ii < 8; ii++)
		for (int jj = 0; jj < 8; jj++)
			matrix->chan[ii][jj] = idctMatrix[ii][jj];
}

//������� ���������������� 4:2:2
void sub422(double** chan, int height, int width)
{
	for (unsigned int i = 0; i < height; i += 2)
	{
		if (i == height - 1)
			break;
		for (unsigned int j = 0; j < width; j += 2)
		{
			if (j == width - 1)
				break;
			float avg = chan[i][j] + chan[i][j + 1] + chan[i + 1][j] + chan[i + 1][j + 1];
			chan[i][j] = chan[i][j + 1] = chan[i + 1][j] = chan[i + 1][j + 1] = avg * 0.25;
		}
	}
}

//������� ���������������� 4:2:0 
void sub420(double** chan, int height, int width)
{
	for (unsigned int i = 0; i < height; i++)
	{
		for (unsigned int j = 0; j < width; j += 2)
		{
			if (j == width - 1)
				break;
			float avg = chan[i][j] + chan[i][j + 1];
			chan[i][j] = chan[i][j + 1] = avg * 0.5;
		}
	}
}

//������� �������������� RGB � YUV
void YUVfromRGB(RGBQUAD** rgb, double** Y, double** Cb, double** Cr, unsigned int height, unsigned int width)
{
	for (unsigned int i = 0; i < height; i++)
	{
		for (unsigned int j = 0; j < width; j++)
		{
			Y[i][j] = 0.299 * rgb[i][j].rgbRed + 0.587 * rgb[i][j].rgbGreen + 0.114 * rgb[i][j].rgbBlue - 128;
			Cb[i][j] = -0.169 * rgb[i][j].rgbRed - 0.331 * rgb[i][j].rgbGreen + 0.5 * rgb[i][j].rgbBlue;
			Cr[i][j] = 0.5 * rgb[i][j].rgbRed - 0.419 * rgb[i][j].rgbGreen - 0.081 * rgb[i][j].rgbBlue;

			
		}
	}

}

//������� �������������� YUV � RGB
void RGBfromYUV(RGBQUAD** rgb, double** Y, double** Cb, double** Cr, unsigned int height, unsigned int width)
{
	for (unsigned int i = 0; i < height; i++)
	{
		for (unsigned int j = 0; j < width; j++)
		{
			rgb[i][j].rgbRed = (Y[i][j] + 128) + Cr[i][j] * 1.402;
			if (rgb[i][j].rgbRed > 255) rgb[i][j].rgbRed = 255;
			if (rgb[i][j].rgbRed < 0) rgb[i][j].rgbRed = 0;
			rgb[i][j].rgbGreen = (Y[i][j] + 128) + Cb[i][j] * -0.34414 + Cr[i][j] * -0.71414;
			if (rgb[i][j].rgbGreen > 255) rgb[i][j].rgbGreen = 255;
			if (rgb[i][j].rgbGreen < 0) rgb[i][j].rgbGreen = 0;
			rgb[i][j].rgbBlue = (Y[i][j] + 128) + Cb[i][j] * 1.772;
			if (rgb[i][j].rgbBlue > 255) rgb[i][j].rgbBlue = 255;
			if (rgb[i][j].rgbBlue < 0) rgb[i][j].rgbBlue = 0;
		}
	}
}

//������� ������ ����� ���������
unsigned char bitextract(const unsigned int byte, const unsigned int mask) {
	if (mask == 0) {
		return 0;
	}

	// ����������� ���������� ������� ��� ������ �� �����
	int
		maskBufer = mask,
		maskPadding = 0;

	while (!(maskBufer & 1)) {
		maskBufer >>= 1;
		maskPadding++;
	}

	// ���������� ����� � ��������
	return (byte & mask) >> maskPadding;
}

int menu()
{
	int choice;

	cout << endl <<
		"\n\t  ____________________________________" << endl <<
		"\t |1 - ���������� bmp ����             |" << endl <<
		"\t |2 - ����������� bin ���� � bmp      |" << endl <<
		"\t |3 - �����                           |" << endl <<
		"\t |____________________________________|\n\n" << endl << endl;

	cout << "\t\t�������� �����\n" << endl;

	choice = _getwch();

	return choice;
}

//����� ������ ��������
void printTree(Node* root, unsigned k)
{
	if (root != NULL)
	{
		printTree(root->left, k + 3);

		for (unsigned i = 0; i < k; i++)
		{
			cout << " ";
		}

		if (root->symbol != INT_MAX) cout << root->count << " :(" << root->symbol << ")" << endl;
		else cout << root->count << endl;
		printTree(root->right, k + 3);
	}
}

vector<int> convert(map<int, int> m)
{
	vector<int> temp2;
	for (auto &item : m)
	{
		temp2.push_back(item.first);
		temp2.push_back(item.second);
	}
	return temp2;
}
